package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

class TestQueue {

	Queue<Integer> numeros;

	/**
	 * crea el el queue de numeros
	 */
	public void setUpEscenario1(){
		numeros = new Queue<Integer>();
	}

	/**
	 * Prueba que la cola siempre se inicie en cero y adem�s prueba la funci�n size
	 */
	@Test
	public void sizeTest() {
		setUpEscenario1();
		int siz = 0;
		assertEquals("el tama�o del Queue no es el correcto",siz ,numeros.size());
	}

	/**
	 * Prueba que la funci�n queue adicione los elementos a la cola correctamente.
	 */
	@Test
	public void queueTest() {
		setUpEscenario1();
		numeros.enqueue(2);
		numeros.enqueue(4);
		numeros.enqueue(5);
		int siz = 3;
		assertEquals("el tama�o del queue no es correcto",siz, numeros.size());
	}

	/**
	 * Prueba que la funci�n dequeue este quitando elementos de la cola
	 */
	@Test
	public void dequeue() {
		setUpEscenario1();
		numeros.enqueue(2);
		numeros.enqueue(3);
		numeros.enqueue(4);
		numeros.dequeue();
		int siz = 2;
		assertEquals("no se eliminaron elementos",siz, numeros.size());
		siz = 1;
		numeros.dequeue();
		assertEquals("no se eliminaron elementos", siz, numeros.size());
	}

	/**
	 * prueba que la funci�n is Empty funcione correctamente con el queue y el dequeue
	 */
	@Test
	public void emptyTest2() {
		setUpEscenario1();
		numeros.enqueue(4);
		assertTrue("la cola no deber�a estar vacia", !numeros.isEmpty());
		numeros.dequeue();
		assertTrue("la cola deber�a estar vacia", numeros.isEmpty());
	}
	/**
	 * Prueba que el Queue se cree en el orden correcto. 
	 */
	@Test
	public void correctOrder() {
		setUpEscenario1();
		for(int i = 0; i < 8; i ++) {
			numeros.enqueue(i);
		}
		for(int i = 0; i < 8; i++) {
			assertTrue("el orden de la cola no es el correcto", numeros.dequeue() == i);
		}
		numeros.enqueue(2);
		int n = 2;
		assertTrue("el numero espereado no es el correcto", n == numeros.dequeue());
	}

	/**
	 * Prueba que el iterador devuelva la cola en el orden correcto
	 */
	@Test
	public void iteradorTest() {
		setUpEscenario1();
		for(int i = 0; i < 8; i ++) {
			numeros.enqueue(i);
		}
		Iterator<Integer> ite = numeros.iterator();
		assertNotNull("el iterador es nulo", ite);
		assertTrue("el iterador deber�a tener un siguiente elemento", ite.hasNext());
		int m = 0;
		for(Integer ent : numeros) {
			assertTrue("el orden del iterador no es correcto", m == ent);
			m++;
		}
	}
}
