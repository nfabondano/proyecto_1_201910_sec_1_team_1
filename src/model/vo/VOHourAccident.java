package model.vo;

import java.text.DecimalFormat;

/**
 * Clase que representa las infracciones y accidentes de cierta hora
 * @author Nicol�s
 *
 */
public class VOHourAccident {

	/**
	 * Porcentaje de accidentes por infracci�n de la hora.
	 */
	private int porcentaje;

	/**
	 * Cantidad de accidentes ocurridos en esa hora.
	 */
	private double accidentes;

	/**
	 * Constructor del contenedor de la hora.
	 * @param pAccidentes
	 * @param pInfracciones
	 */
	public VOHourAccident(double pAccidentes) {
		accidentes = pAccidentes;
	}

	/**
	 * Calcula el porcentaje
	 */
	public void calcularPorcentaje(double total) {
		if(total!=0)
			porcentaje = (int) ((accidentes/total)*100);
		else
			porcentaje = 0;
	}

	/**
	 * Retorna el porcentaje
	 */
	public int darPorcentaje() {
		return porcentaje;
	}
}



