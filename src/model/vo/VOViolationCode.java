package model.vo;

/**
 * Clase que representa el tipo de infraccion y las características del fineAMT de este
 * @author Nicolás
 *
 */
public class VOViolationCode {

	/**
	 * Tipo de infracción
	 */
	private String violationCode;

	/**
	 * Promedio de fineAMT
	 */
	private double fineAMT;

	/**
	 * Constructor de la clase del tipo de infracción
	 * @param pCode
	 * @param pFineAMT
	 * @param pDesviacion
	 */
	public VOViolationCode(String pCode, double pFineAMT) {
		violationCode = pCode;
		fineAMT = pFineAMT;
	}

	/**
	 * Retorna el tipo de infracción.
	 * @return el tipo de infracción.
	 */
	public String getCode(){
		return violationCode;
	}

	/**
	 * Retorna el promedio de fineAMT de la clase
	 * @return el promedio de fineAMT de la clase.
	 */
	public double getFineAMT() {
		return fineAMT;
	}

}

