package model.vo;

public class VODaylyStatistic {

	//--------------------------------Atributos-------------------------------//


	/**
	 * fecha unica en la que se dan los accidentes
	 */
	private String date;

	/**
	 * numero de accidentes que se dieron en la fecha
	 */
	private int nAccident;

	/**
	 * numero de infracciones que se dieron en la fecha 
	 */
	private int nInfraction;

	/**
	 * el valor total de las infracciones de ese dia
	 */
	private int fineAMT;

	//-------------------------------------------Constructor----------------------//

	/**
	 * M�todo constructor que inicializa los atributos con los valores pasados por parametros
	 * @param pDate fecha de la cual se guardan las estadisticas. pDate != null
	 * @param pNAccidentes numero de accidentes que se registraron ese dia. pNAccidentes >= 0
	 * @param pNInfracciones numero de infracciones que se registraron ese d�a. pNInfracciones > 0
	 * @param pFineAMT valor total de las infracciones. pFineAMT > 0
	 */
	public VODaylyStatistic(String pDate, int pNAccidentes,int pNInfracciones, int pFineAMT) {
		date = pDate;
		nAccident = pNAccidentes;
		nInfraction = pNInfracciones;
		fineAMT = pFineAMT;
	}

	/**
	 * devuelve la fecha de la cual se estan obteniendo las estadisticas
	 * @return fecha de las estadisticas
	 */
	public String getDate(){
		return date;
	}

	/**
	 * devuelve el numero de accidentes en la fecha de las estadisticas
	 * @return numero de accidentes en la fecha de las estadisticas
	 */
	public int getNAccident() {
		return nAccident;
	}

	/**
	 * devuelve el numero de infracciones en la fecha de las estadisticas
	 * @return numero de infracciones en la fecha de las estadisticas
	 */
	public int getNInfraction() {
		return nInfraction;
	}

	/**
	 * devuelve el valor total de las infracciones en la fecha de las estadisticas
	 * @return el valor total de las infracciones en la fecha de las estadisticas
	 */
	public int getFineAMT() {
		return fineAMT;
	}
}

