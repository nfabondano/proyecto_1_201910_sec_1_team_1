package model.vo;

import java.sql.RowId;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>{

	/**
	 * ObjectID del VOMovingViolations.
	 */
	private int objectID;

	/**
	 * Row del VOMovingViolations.
	 */
	private String ROW;

	/**
	 * Location del VOMovingViolations.
	 */
	private String location;

	/**
	 * AdressID del VOMovingViolations.
	 */
	private String adressID;

	/**
	 * StreetSegid del VOMovingViolations.
	 */
	private String streetSegid;

	/**
	 * XCOORD del VOMovingViolations.
	 */
	private double XCOORD;

	/**
	 * YCOORD del VOMovingViolations.
	 */
	private double YCOORD;

	/**
	 * TicketType del VOMovingViolations.
	 */
	private String ticketType;

	/**
	 * FineAMT del VOMovingViolations.
	 */
	private int fineAMT;

	/**
	 * TotalPaid del VOMovingViolations.
	 */
	private double totalPaid;

	/**
	 * Penalty1 del VOMovingViolations.
	 */
	private int penalty1;

	/**
	 * Penalty2 del VOMovingViolations.
	 */
	private int penalty2;

	/**
	 * AccidentIndicator del VOMovingViolations.
	 */
	private String accidentIndicator;

	/**
	 * AgencyID del VOMovingViolations
	 */
	private int agencyID;

	/**
	 * TicketIssueDate del VOMovingViolations.
	 */
	private String ticketIssueDate;

	/**
	 * Fecha de la infraccion.
	 */
	private LocalDateTime fecha;

	/**
	 * ViolationCode del VOMovingViolations.
	 */
	private String violationCode;

	/**
	 * ViolationDescription del VOMovingViolations.
	 */
	private String violationDescription;

	/**
	 * ROWID del VOMovingViolations.
	 */
	private String ROWID;

	/**
	 * Constructor del VOMovingViolations.
	 * @param pLine Linea para construir el VOMovingVIolations
	 */
	public VOMovingViolations( String[] pLine) {
		objectID = Integer.parseInt(pLine[0]);
		ROW = pLine[1];
		location = pLine[2];
		adressID = pLine[3];	
		streetSegid = pLine[4];
		XCOORD = Double.parseDouble(pLine[5]);
		YCOORD = Double.parseDouble(pLine[6]);
		ticketType = pLine[7];
		fineAMT = Integer.parseInt(pLine[8]);
		totalPaid = Double.parseDouble(pLine[9]);
		penalty1 = Integer.parseInt(pLine[10]);

		try{
			penalty2 = Integer.parseInt(pLine[11]);
		}catch(Exception e){
			penalty2 = 0;
		}
		accidentIndicator = pLine[12];

		int length = pLine.length;
		if(length == 18) {
			try {
				agencyID = Integer.parseInt(pLine[13]);
			}
			catch(Exception e) {

			}
		}

		ticketIssueDate = pLine[length-4];
		fecha = convertirFecha_Hora_LDT(ticketIssueDate);
		violationCode = pLine[length-3];
		violationDescription = pLine[length-2];
		ROWID = pLine[length-1];
	}

	public VOMovingViolations(){

	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int getObjectID() {
		return objectID;
	}	

	/**
	 * @return Row del VOMovingViolations.
	 */
	public String getROW() {
		return ROW;
	}


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return AdressID del VOMovingViolations.
	 */
	public String getAdressID(){ 
		return adressID;
	}

	/**
	 * @return StreetSgid del VOMovingViolations.
	 */
	public String getStreetSgid() {
		return streetSegid;
	}

	/**
	 * @return XCOORD del VOMovingViolations.
	 */
	public double getXCOORD(){
		return XCOORD;
	}

	/**
	 * @return YCOORD del VOMovingViolations.
	 */
	public double getYCOORD(){ 
		return YCOORD;
	}

	/**
	 * @return TicketType del VOMovingViolations.
	 */
	public String getTicketType(){ 
		return ticketType;
	}

	/**
	 * @return FineAMT del VOMovingViolations.
	 */
	public int getfineAMT(){
		return fineAMT;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public double getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return Penalty1 del VOMovingViolations.
	 */
	public int penalty1(){ 
		return penalty1;
	}

	/**
	 * @return Penalty2 del VOMovingViolations.
	 */
	public int penalty2(){ 
		return penalty2;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String getAccidentIndicator() {
		return accidentIndicator;
	}

	/**
	 * @return agencyID - AgencyID del VOMovingViolations
	 */
	public int getAgencyID() {
		return agencyID;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	/**
	 * @return Fecha cuando se puso la infracción .
	 */
	public LocalDateTime getFecha() {
		return fecha;
	}

	/**
	 * @return violationCode - Devuelve el c�digo de violaci�n.
	 */
	public String getViolationCode() {
		return violationCode;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		return violationDescription;
	}

	/**
	 * @return ROWID del VOMovingViolations.
	 */
	public String getROWID(){ 
		return ROWID;
	}


	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-mm-dd'T'HH:mm:ss'Z'"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora) {
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}


	/**
	 * M�todo de comparaci�n de la clase.
	 */
	public int compareTo(VOMovingViolations pComparar) {
		return fecha.compareTo(pComparar.fecha);
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n hora de la fecha
	 */
	public static class ComparatorHour implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por hora de la fecha
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			return v1.fecha.toLocalTime().compareTo(v2.fecha.toLocalTime());
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n fecha
	 */
	public static class ComparatorDate implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por fecha
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			return v1.compareTo(v2);
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n violationCode
	 */
	public static class ComparatorViolationCode implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por violationCode
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			return v1.violationCode.compareToIgnoreCase(v2.violationCode);
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n violationDesc
	 */
	public static class ComparatorViolationDesc implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por violationDesc
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			return v1.violationDescription.compareToIgnoreCase(v2.violationDescription);
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n objectID
	 */
	public static class ComparatorObjectID implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por ObjectID
		 */
		public int compare(VOMovingViolations v1, VOMovingViolations v2) {
			if(v1.objectID > v2.objectID) return 1;
			else if(v1.objectID < v2.objectID) return -1;
			else return 0;
		}
	}

	/**
	 * Clase auxiliar utilizada para comparar seg�n streetSegId
	 */
	public static class ComparadorStreetSegId implements Comparator<VOMovingViolations>{

		/**
		 * Comparaci�n de dos objetos por StreetSegId
		 * M�todo que compara de manera ascendente dos VOMovingViolations seg�n su 
		 * streetSegId. si estos llegan a ser iguales, compara seg�n su fecha
		 */
		public int compare(VOMovingViolations o1, VOMovingViolations o2) {
			if(o1.streetSegid.compareToIgnoreCase(o2.streetSegid) > 0) 
				return 1;
			if(o1.streetSegid.compareTo(o2.streetSegid) < 0)
				return -1;
			return o1.compareTo(o2);
		}

	}

	/**
	 * M�todo que devuelve la identificaci�n de la infracci�n en string.
	 * @return identificaci�n de la infracci�n en string
	 */
	public String toString(){
		return objectID + "-" + ticketIssueDate;
	}

}
