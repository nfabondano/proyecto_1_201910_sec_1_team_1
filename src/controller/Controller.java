package controller;

import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Scanner;

import com.opencsv.CSVReader;

import model.data_structures.IOrderedResizingArrayList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.OrderedResizingArrayList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOHourAccident;
import model.vo.VOMovingViolations;
import model.vo.VOViolationCode;
import view.MovingViolationsManagerView;

public class Controller {

	/**
	 * Constante del archivo de enero.
	 */
	public final static String ENERO = "./data/Moving_Violations_Issued_In_January_2018.csv";

	/**
	 * Constante del archivo de febrero.
	 */
	public final static String FEBRERO = "./data/Moving_Violations_Issued_In_February_2018.csv";

	/**
	 * Constante del archivo de marzo.
	 */
	public final static String MARZO = "./data/Moving_Violations_Issued_In_March_2018.csv";

	/**
	 * Constante del archivo de abril.
	 */
	public final static String ABRIL = "./data/Moving_Violations_Issued_In_April_2018.csv";

	/**
	 * Constante del archivo de mayo.
	 */
	public final static String MAYO = "./data/Moving_Violations_Issued_In_May_2018.csv";

	/**
	 * Constante del archivo de junio.
	 */
	public final static String JUNIO = "./data/Moving_Violations_Issued_In_June_2018.csv";

	/**
	 * Constante del archivo de julio.
	 */
	public final static String JULIO = "./data/Moving_Violations_Issued_In_July_2018.csv";

	/**
	 * Constante del archivo de agosto.
	 */
	public final static String AGOSTO = "./data/Moving_Violations_Issued_In_August_2018.csv";

	/**
	 * Constante del archivo de septiembre.
	 */
	public final static String SEPTIEMBRE = "./data/Moving_Violations_Issued_In_September_2018.csv";

	/**
	 * Constante del archivo de octubre.
	 */
	public final static String OCTUBRE = "./data/Moving_Violations_Issued_In_October_2018.csv";

	/**
	 * Constante del archivo de noviembre.
	 */
	public final static String NOVIEMBRE = "./data/Moving_Violations_Issued_In_November_2018.csv";

	/**
	 * Constante del archivo de diciembre.
	 */
	public final static String DICIEMBRE = "./data/Moving_Violations_Issued_In_December_2018.csv";
	/**
	 * forma de conectarse con la clase que imprime informaci�n a la consola
	 */
	private MovingViolationsManagerView view;

	/**	
	 * Lista donde se van a cargar los datos de los archivos
	 */
	private IOrderedResizingArrayList<VOMovingViolations> movingViolationsArray;

	/**
	 * Lista donde se van a cargar los datos de los archivos
	 */
	private IOrderedResizingArrayList<VOMovingViolations> movingViolationsArrayOrdered;	

	/**
	 * Archivos para leer en csv.
	 */
	private String[] csvData;

	/**
	 * Comparador por fecha.
	 */
	private Comparator<VOMovingViolations> comparadorFecha;

	/**
	 * Comparador por objectId.
	 */
	private Comparator<VOMovingViolations> comparadorObject;

	/**
	 * Comparador por violation code.
	 */
	private Comparator<VOMovingViolations> comparadorCode;

	/**
	 * Comparador por hora.
	 */
	private Comparator<VOMovingViolations> comparadorHora;

	/**
	 * Comparador por violation description.
	 */
	private Comparator<VOMovingViolations> comparadorDesc;

	/**
	 * Comparador por streetSegidIdInvertido
	 */
	private Comparator<VOMovingViolations> comparadorStreetSegId;

	/**
	 * Tama�o del arreglo
	 */
	private int size;

	/**
	 * Constructor del controlador
	 */
	public Controller() {
		view = new MovingViolationsManagerView();
		csvData = new String[12];
		csvData[0] = ENERO; 	csvData[4] = MAYO; 		csvData[8] = SEPTIEMBRE;
		csvData[1] = FEBRERO; 	csvData[5] = JUNIO; 	csvData[9] = OCTUBRE;
		csvData[2] = MARZO; 	csvData[6] = JULIO; 	csvData[10] = NOVIEMBRE;
		csvData[3] = ABRIL; 	csvData[7] = AGOSTO; 	csvData[11] = DICIEMBRE;
		comparadorFecha = new VOMovingViolations.ComparatorDate();
		comparadorObject = new VOMovingViolations.ComparatorObjectID();
		comparadorCode = new VOMovingViolations.ComparatorViolationCode();
		comparadorDesc = new VOMovingViolations.ComparatorViolationDesc();
		comparadorHora = new VOMovingViolations.ComparatorHour();
		comparadorStreetSegId = new VOMovingViolations.ComparadorStreetSegId();
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				view.printMessage("Ingrese el cuatrimestre (1, 2 o 3)");
				int numeroCuatrimestre = sc.nextInt();
				loadMovingViolations(numeroCuatrimestre);
				break;

			case 1:
				IOrderedResizingArrayList<VOMovingViolations> isUnique = verifyObjectIDIsUnique();
				if(isUnique.getSize() == 0)
					view.printMessage("El objectId es unico");
				else{
					String respuesta = "";
					for(int i = 0; i < isUnique.getSize(); i++){
						respuesta += isUnique.getElement(i).getObjectID();
						respuesta += " ";
					}
					view.printMessage("Los objectId repetidos son: " + respuesta);
				}
				break;

			case 2:

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2017-03-28T15:30:20)");
				LocalDateTime fechaInicialReq2A = convertirFecha_Hora_LDT(sc.next()+".000Z");

				view.printMessage("Ingrese la fecha con hora final (Ej : 2017-03-28T15:30:20)");
				LocalDateTime fechaFinalReq2A = convertirFecha_Hora_LDT(sc.next() + ".000Z");

				IQueue<VOMovingViolations> resultados2 = getMovingViolationsInRange(fechaInicialReq2A, fechaFinalReq2A);

				view.printMovingViolationsReq2(resultados2);

				break;

			case 3:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode3 = sc.next();

				double [] promedios3 = avgFineAmountByViolationCode(violationCode3);
				DecimalFormat numberFormat = new DecimalFormat("#.00");
				view.printMessage("FINEAMT promedio sin accidente: " + numberFormat.format(promedios3[0]) + ", con accidente:" + numberFormat.format(promedios3[1]));
				break;


			case 4:

				view.printMessage("Ingrese el ADDRESS_ID");
				String addressId4 = sc.next();

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 28/03/2017)");
				LocalDate fechaInicialReq4A = convertirFecha(sc.next());

				view.printMessage("Ingrese la fecha con hora final (Ej : 28/03/2017)");
				LocalDate fechaFinalReq4A = convertirFecha(sc.next());

				IStack<VOMovingViolations> resultados4 = getMovingViolationsAtAddressInRange(addressId4, fechaInicialReq4A, fechaFinalReq4A);

				view.printMovingViolationsReq4(resultados4);

				break;

			case 5:
				view.printMessage("Ingrese el limite inferior de FINEAMT  (Ej: 50)");
				double limiteInf5 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de FINEAMT  (Ej: 50)");
				double limiteSup5 = sc.nextDouble();

				IQueue<VOViolationCode> violationCodes = violationCodesByFineAmt(limiteInf5, limiteSup5);
				view.printViolationCodesReq5(violationCodes);
				break;

			case 6:

				view.printMessage("Ingrese el limite inferior de TOTALPAID (Ej: 200)");
				double limiteInf6 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de TOTALPAID (Ej: 200)");
				double limiteSup6 = sc.nextDouble();

				view.printMessage("Ordenar Ascendentmente: (Ej: true)");
				boolean ascendente6 = sc.nextBoolean();				

				IStack<VOMovingViolations> resultados6 = getMovingViolationsByTotalPaid(limiteInf6, limiteSup6, ascendente6);
				view.printMovingViolationReq6(resultados6);
				break;

			case 7:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial7 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal7 = sc.nextInt();

				IQueue<VOMovingViolations> resultados7 = getMovingViolationsByHour(horaInicial7, horaFinal7);
				view.printMovingViolationsReq7(resultados7);
				break;

			case 8:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode8 = sc.next();

				double [] resultado8 = avgAndStdDevFineAmtOfMovingViolation(violationCode8);

				view.printMessage("FINEAMT promedio: " + resultado8[0] + ", desviación estandar:" + resultado8[1]);
				break;

			case 9:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial9 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal9 = sc.nextInt();

				int resultado9 = countMovingViolationsInHourRange(horaInicial9, horaFinal9);

				view.printMessage("Número de infracciones: " + resultado9);
				break;

			case 10:
				view.printMovingViolationsByHourReq10(datosTabla());
				break;

			case 11:
				view.printMessage("Ingrese la fecha inicial (Ej : 28/03/2017)");
				LocalDate fechaInicial11 = convertirFecha(sc.next());

				view.printMessage("Ingrese la fecha final (Ej : 28/03/2017)");
				LocalDate fechaFinal11 = convertirFecha(sc.next());

				double resultados11 = totalDebt(fechaInicial11, fechaFinal11);
				view.printMessage("Deuda total "+ resultados11);
				break;

			case 12:	
				view.printTotalDebtbyMonthReq12(datosDeudaAcumulada());

				break;

			case 13:	
				fin=true;
				sc.close();
				break;
			}
		}

	}

	/**
	 * M�todo que carga las infracciones
	 * @param numeroCuatrimestre el cuatrimetre que se debe cargar numeroCuatrimestre = {1,2,3}
	 */
	public void loadMovingViolations(int numeroCuatrimestre) {
		if(numeroCuatrimestre == 1)size = 373550;
		if(numeroCuatrimestre == 1)size = 484960;
		else size = 405210;
		movingViolationsArray = new OrderedResizingArrayList<VOMovingViolations>(size);
		movingViolationsArrayOrdered = new OrderedResizingArrayList<VOMovingViolations>(size);		
		try {
			int mesInicial = (numeroCuatrimestre-1)*4;
			for(int i = 0; i<4; i++) {
				int mesActual = mesInicial + i;
				File m = new File(csvData[mesActual]);
				FileReader nm = new FileReader(m);
				CSVReader reader = new CSVReader(nm);
				String nextLine[];
				nextLine = reader.readNext();
				while((nextLine = reader.readNext()) != null) {
					VOMovingViolations a�adir = new VOMovingViolations(nextLine);
					movingViolationsArray.add(a�adir);
					movingViolationsArrayOrdered.add(a�adir);
				}
				reader.close();
			}
			movingViolationsArrayOrdered.sort(comparadorFecha);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * M�todo que devuelve aquellos objectIDs que esten repetidos en las infracciones
	 * @return devuelve aquellos objectIDs que esten repetidos en las infracciones
	 */
	public IOrderedResizingArrayList<VOMovingViolations> verifyObjectIDIsUnique() {
		IOrderedResizingArrayList<VOMovingViolations> answer = new OrderedResizingArrayList<VOMovingViolations>(10);
		movingViolationsArray.sort(comparadorObject);
		int size = movingViolationsArray.getSize();
		for(int i = 0; i < size - 1; i++){
			if(movingViolationsArray.getElement(i).getObjectID() == movingViolationsArray.getElement(i+1).getObjectID())
				answer.add(movingViolationsArray.getElement(i));
		}
		return answer;
	}

	/**
	 * Devuelve las infracciones que se dieron en las fechas especificadas
	 * pre: los datos de los meses se han cargado.
	 * @param fechaInicial fecha desde la cual se quiren saber las infracciones fechaIncial != null
	 * @param fechaFinal fecha hasta la cual se quieren saber las infracciones fechaFinal != null
	 * @return Devuelve las infracciones que se dieron en las fechas especificadas
	 */
	public IQueue<VOMovingViolations> getMovingViolationsInRange(LocalDateTime fechaInicial,
			LocalDateTime fechaFinal) {
		IQueue<VOMovingViolations> answer = new Queue<VOMovingViolations>();
		int size = movingViolationsArrayOrdered.getSize();
		int hi = movingViolationsArrayOrdered.getSize()-1;
		int lo = 0;
		int mid = lo + (hi -lo)/2;
		while(lo <= hi) {
			if(movingViolationsArrayOrdered.getElement(mid).getFecha().compareTo(fechaInicial) == 0  || (movingViolationsArrayOrdered.getElement(mid).getFecha().compareTo(fechaInicial) > 0
					&& (mid -1 == -1  || movingViolationsArrayOrdered.getElement(mid-1).getFecha().compareTo(fechaInicial) < 0 ))){
				break;
			}
			else if(movingViolationsArrayOrdered.getElement(mid).getFecha().compareTo(fechaInicial) > 0)
				hi = mid -1;
			else 
				lo = mid + 1;
			mid = lo + (hi -lo)/2;
		}

		while(true) {
			if(mid < size && !(movingViolationsArrayOrdered.getElement(mid).getFecha().compareTo(fechaFinal) >  0))
				answer.enqueue(movingViolationsArrayOrdered.getElement(mid++));
			else
				break;
		}
		return answer;
	}

	/**
	 * calcula el FineAMT promedio de las infracciones con el violationCode pasado por parametro
	 * cuando hubo y cuando no hubo accidente
	 * @param violationCode3 el codigo de violaci�n con el que se buscan las infracciones
	 * @return el FineAMT promedio de las infracciones con el violationCode pasado por parametro
	 * cuando hubo y cuando no hubo accidente
	 */
	public double[] avgFineAmountByViolationCode(String violationCode3) {
		movingViolationsArray.sort(comparadorCode);
		double totalWithoutMoney = 0;
		double totalWithMoney = 0;
		double totalWithout = 0;
		double totalWith = 0;
		int hi = movingViolationsArray.getSize()-1;
		int lo = 0;
		int mid = lo + (hi -lo)/2;
		while(lo <= hi) {
			if(movingViolationsArray.getElement(mid).getViolationCode().equalsIgnoreCase(violationCode3)){
				break;
			}
			else if(movingViolationsArray.getElement(mid).getViolationCode().compareToIgnoreCase(violationCode3) > 0)
				hi = mid -1;
			else 
				lo = mid + 1;
			mid = lo + (hi -lo)/2;
		}
		int i = mid -1;
		int j = mid + 1;
		while(i >= 0 && movingViolationsArray.getElement(i).getViolationCode().equals(violationCode3)) {
			if(movingViolationsArray.getElement(i).getAccidentIndicator().equals("Yes")) {
				totalWith++;
				totalWithMoney += movingViolationsArray.getElement(i).getfineAMT();
			}
			else {
				totalWithout++;
				totalWithoutMoney += movingViolationsArray.getElement(i).getfineAMT();
			}
			i--;
		}
		while(j < movingViolationsArray.getSize() && movingViolationsArray.getElement(j).getViolationCode().equals(violationCode3)) {
			if(movingViolationsArray.getElement(j).getAccidentIndicator().equals("Yes")) {
				totalWith++;
				totalWithMoney += movingViolationsArray.getElement(j).getfineAMT();
			}
			else {
				totalWithout++;
				totalWithoutMoney += movingViolationsArray.getElement(j).getfineAMT();
			}
			j++;
		}
		if(movingViolationsArray.getElement(mid).getAccidentIndicator().equals("Yes")) {
			totalWith++;
			totalWithMoney += movingViolationsArray.getElement(mid).getfineAMT();
		}
		else {
			totalWithout++;
			totalWithoutMoney += movingViolationsArray.getElement(mid).getfineAMT();
		}
		double ans[] = {totalWithoutMoney/totalWithout, totalWithMoney/totalWith};
		return ans;
	}

	/**
	 * m�todo que devuelve las infracciones que se dieron entre unas fechas y addressID dadas, ordenadas descendentemente con ayuda de un stack
	 * @param addressId lugar donde se cometio la infraccion addressId != null
	 * @param fechaInicial fecha desde la cual se quiren saber las infracciones fechaIncial != null
	 * @param fechaFinal fecha hasta la cual se quieren saber las infracciones fechaFinal != null
	 * @return las infracciones que se dieron entre unas fechas y addressID dadas, ordenadas descendentemente con ayuda de un stack.
	 */
	public IStack<VOMovingViolations> getMovingViolationsAtAddressInRange(String addressId,LocalDate fechaInicial, LocalDate fechaFinal) {
		// TODO Auto-generated method stub
		IStack<VOMovingViolations> answer = new Stack<VOMovingViolations>();
		OrderedResizingArrayList<VOMovingViolations> temp = new OrderedResizingArrayList<VOMovingViolations>(10);
		int size = movingViolationsArrayOrdered.getSize();
		int hi = movingViolationsArrayOrdered.getSize()-1;
		int lo = 0;
		int mid = lo + (hi -lo)/2;
		while(lo <= hi) {
			if(movingViolationsArrayOrdered.getElement(mid).getFecha().toLocalDate().compareTo(fechaInicial) == 0  || (movingViolationsArrayOrdered.getElement(mid).getFecha().toLocalDate().compareTo(fechaInicial) > 0
					&& (mid -1 == -1  || movingViolationsArrayOrdered.getElement(mid-1).getFecha().toLocalDate().compareTo(fechaInicial) < 0 ))){
				break;
			}
			else if(movingViolationsArrayOrdered.getElement(mid).getFecha().toLocalDate().compareTo(fechaInicial) > 0)
				hi = mid -1;
			else 
				lo = mid + 1;
			mid = lo + (hi -lo)/2;
		}
		while(true) {
			if(mid < size && !(movingViolationsArrayOrdered.getElement(mid).getFecha().toLocalDate().compareTo(fechaFinal) >  0)) {
				if(movingViolationsArrayOrdered.getElement(mid).getAdressID().equalsIgnoreCase(addressId))
					temp.add(movingViolationsArrayOrdered.getElement(mid));
				mid++;
			}
			else
				break;
		}
		System.out.println("por aqui tambi�n");
		temp.sort(comparadorStreetSegId);
		for(int i = 0; i < temp.getSize(); i++){
			answer.push(temp.getElement(i));
		}
		return answer;
	}

	/**
	 * M�todo que retorna una cola con los c�digos de violaci�n que tengan el FINEAMT promedio en un rango
	 * @param limiteInf5. limite del rango inferior. limiteInf >= 0.
	 * @param limiteSup5. limite del rango superior. limiteSup >= 0.
	 * @return la cola con los c�digos de violaci�n.
	 */
	public IQueue<VOViolationCode> violationCodesByFineAmt(double limiteInf5, double limiteSup5) {
		IQueue<VOViolationCode> cola = new Queue<VOViolationCode>();
		movingViolationsArray.sort(comparadorCode);
		VOMovingViolations actual = movingViolationsArray.getElement(0);
		String code = actual.getViolationCode();
		double fine = actual.getfineAMT(); int tot = 1;
		for (int i = 1; i < movingViolationsArray.getSize(); i++) {
			actual = movingViolationsArray.getElement(i);
			String codeA = actual.getViolationCode();
			int fineA = actual.getfineAMT();
			if(!codeA.equalsIgnoreCase(code)) {
				fine = fine/tot;
				if(fine <= limiteSup5 && fine >= limiteInf5) {
					VOViolationCode v = new VOViolationCode(code, fine);
					cola.enqueue(v);
				}
				code = codeA;
				fine = fineA;
				tot = 1;
			}
			else {
				fine += fineA;
				tot++;
			}

		}
		return cola;
	}

	/**
	 * Retorna una pila con infracciones donde la cantidad pagada est� en un rango dado y est�n ordenadas por fecha, seg�n el par�metro.
	 * @param limiteInf6. limite inferior del rango de la cantidad pagada.
	 * @param limiteSup6 limite superior del rango de la cantidad pagada.
	 * @param ascendente6 si se desea la cola ordenada ascendentemenete
	 * @return cola ordenada por fecha con cantidad pagada en un rango
	 */
	public IStack<VOMovingViolations> getMovingViolationsByTotalPaid(double limiteInf6, double limiteSup6, boolean ascendente6) {
		IStack<VOMovingViolations> pila = new Stack<VOMovingViolations>();
		for (VOMovingViolations voMovingViolations : movingViolationsArrayOrdered)
			if(voMovingViolations.getTotalPaid() >= limiteInf6 && voMovingViolations.getTotalPaid() <= limiteSup6) 
				pila.push(voMovingViolations);
		if(!ascendente6)
			pila = invertirPila(pila);
		return pila;
	}

	/**
	 * Retorna una pila invertida
	 * @param pila. pila a invertir
	 * @return pila invertida
	 */
	public IStack<VOMovingViolations> invertirPila( IStack<VOMovingViolations> pila ) {
		IStack<VOMovingViolations> pila2 = new Stack<VOMovingViolations>();
		while(!pila.isEmpty())
			pila2.push(pila.pop());
		return pila2;
	}

	/**
	 * Retorna las infracciones que pertenencen a un rango de horas, ordenadas por violationdesc.
	 * @param horaInicial7. Hora inicial del rango.
	 * @param horaFinal7. Hora final del rango.
	 * @return las infracciones que pertenecen al rango de horas, ordenadas por violationdesc.
	 */
	public IQueue<VOMovingViolations> getMovingViolationsByHour(int horaInicial7, int horaFinal7) {
		IQueue<VOMovingViolations> cola = new Queue<VOMovingViolations>();
		movingViolationsArray.sort(comparadorDesc);
		for(VOMovingViolations voMovingViolations : movingViolationsArray) {
			int hora = Integer.parseInt(voMovingViolations.getTicketIssueDate().split("T")[1].split(":")[0]);
			if(horaInicial7 <= hora && hora <= horaFinal7)
				cola.enqueue(voMovingViolations);
		}
		return cola;
	}

	/**
	 * Retorna el promedio y desviaci�n est�ndar del FINEAMT de un violation code
	 * @param violationCode8. Violation code a examinar
	 * @return el promedio y desviaci�n est�ndar del FINEAMT del violation code.
	 */
	public double[] avgAndStdDevFineAmtOfMovingViolation(String violationCode8) {
		movingViolationsArray.sort(comparadorCode);
		int pos = 0;
		VOMovingViolations actual = movingViolationsArray.getElement(pos);
		while(!violationCode8.equalsIgnoreCase(actual.getViolationCode())) {
			pos++;
			actual = movingViolationsArray.getElement(pos);
		}
		int posI = pos;
		int cont = 0; double prom = 0;
		while(violationCode8.equalsIgnoreCase(actual.getViolationCode())) {
			pos++; cont++;
			prom+=actual.getfineAMT();
			actual = movingViolationsArray.getElement(pos);
		}
		prom = prom/cont;
		double var = 0;
		actual = movingViolationsArray.getElement(posI);
		while(violationCode8.equalsIgnoreCase(actual.getViolationCode())) {
			posI++;
			var+= Math.abs(actual.getfineAMT()-prom);
			actual = movingViolationsArray.getElement(posI);
		}
		var = var/cont;
		return new double [] {prom , var};
	}

	/**
	 * Retorna el n�mero de infracciones en un rango de horas.
	 * @param horaInicial9
	 * @param horaFinal9
	 * @return el n�mero de infracciones en el rango de horas.
	 */
	public int countMovingViolationsInHourRange(int horaInicial9, int horaFinal9) {
		int cont = 0;
		for (VOMovingViolations voMovingViolations : movingViolationsArray) {
			int hora = voMovingViolations.getFecha().getHour(); 
			if(horaInicial9 <= hora && hora <= horaFinal9) 
				cont++;
		}
		return cont; 
	}

	/**
	 * Retorna un arreglo con los datos para generar la tabla ASCII.
	 * @return El arreglo con los datos para generar la tabla ASCII.
	 */
	public VOHourAccident[] datosTabla() {
		VOHourAccident[] horas = new VOHourAccident[24];
		int horaActual = 0;
		movingViolationsArray.sort(comparadorHora);
		int contAcc = 0;int total=0;
		for(int i = 0; i<movingViolationsArray.getSize(); i++) {
			VOMovingViolations actual = movingViolationsArray.getElement(i);
			int hora = actual.getFecha().getHour();
			if(hora != horaActual) {
				total += contAcc;
				horas[horaActual] = new VOHourAccident(contAcc);
				contAcc = 0; total = 0;
				horaActual = hora;
			}
			if(actual.getAccidentIndicator().equals("Yes"))
				contAcc++;
		}
		total += contAcc;
		horas[horaActual] = new VOHourAccident(contAcc);
		for (VOHourAccident voHourAccident : horas) {
			voHourAccident.calcularPorcentaje(total);
		}
		return horas;
	} 

	/**
	 * calcula la deuda total del FINEAMT de los penalties y del totalPaid de las infracciones
	 * entre las fecha especificadas
	 * pre: los datos de los meses se han cargado
	 * @param fechaInicial fecha desde la cual se quiren saber las infracciones fechaIncial != null
	 * @param fechaFinal fecha hasta la cual se quieren saber las infracciones fechaFinal != null
	 * @return la deuda total del FINEAMT de los penalties y del totalPaid de las infracciones
	 * entre las fecha especificadas
	 */
	public long totalDebt(LocalDate fechaInicial, LocalDate fechaFinal) {
		long answer = 0;
		int size = movingViolationsArrayOrdered.getSize();
		int hi = movingViolationsArrayOrdered.getSize()-1;
		int lo = 0;
		int mid = lo + (hi -lo)/2;
		while(lo <= hi) {
			if(movingViolationsArrayOrdered.getElement(mid).getFecha().toLocalDate().compareTo(fechaInicial) == 0  || (movingViolationsArrayOrdered.getElement(mid).getFecha().toLocalDate().compareTo(fechaInicial) > 0
					&& (mid -1 == -1  || movingViolationsArrayOrdered.getElement(mid-1).getFecha().toLocalDate().compareTo(fechaInicial) < 0 ))){
				break;
			}
			else if(movingViolationsArrayOrdered.getElement(mid).getFecha().toLocalDate().compareTo(fechaInicial) > 0)
				hi = mid -1;
			else 
				lo = mid + 1;
			mid = lo + (hi -lo)/2;
		}

		while(true) {
			if(mid < size && !(movingViolationsArrayOrdered.getElement(mid).getFecha().toLocalDate().compareTo(fechaFinal) >  0)) {
				answer += movingViolationsArrayOrdered.getElement(mid).getfineAMT() + movingViolationsArrayOrdered.getElement(mid).getTotalPaid() + movingViolationsArrayOrdered.getElement(mid).penalty2()
						+ movingViolationsArrayOrdered.getElement(mid).penalty1();
				mid++;
			}
			else
				break;
		}
		return answer;
	}

	/**
	 * M�todo que guarda la primera y ultima fecha de cada mes y las devuelve en una matriz
	 * pre: los datos de los diferentes meses han sido cargados 
	 * @return la matriz que contiene la primera y ultima fecha de cada mes.
	 */
	private LocalDate[][] particiones(){
		LocalDate answer[][] = new LocalDate[4][2];
		answer[0][0] = movingViolationsArrayOrdered.getElement(0).getFecha().toLocalDate();
		LocalDate last = movingViolationsArrayOrdered.getElement(0).getFecha().toLocalDate();
		int size = movingViolationsArrayOrdered.getSize();
		int last1 = 0;
		for(int i = 0; i < size; i++) {
			if(!movingViolationsArrayOrdered.getElement(i).getFecha().toLocalDate().getMonth().equals(last.getMonth())) {
				answer[last1++][1] = movingViolationsArrayOrdered.getElement(i-1).getFecha().toLocalDate();
				answer[last1][0] = movingViolationsArrayOrdered.getElement(i).getFecha().toLocalDate();
				last = movingViolationsArrayOrdered.getElement(i).getFecha().toLocalDate();
			}
			if(i == size -1) {
				answer[last1][1] = movingViolationsArrayOrdered.getElement(i).getFecha().toLocalDate();
			}

		}
		return answer;
	}

	/**
	 * M�todo que genera un numero con el primer digito igual al numero pasado por parametro 
	 * y el resto de ceros
	 * @param number numero con el cual se genera la respuesta
	 * @return un numero donde el primer digito es igual al numero pasado por parametro y 
	 * el resto del numero son ceros
	 */
	private long numberGenerator(long number) {
		long numberDivisions = 0;
		long lastDigit = -1;
		while(number != 0) {
			lastDigit = number%10;
			number = number / 10;
			numberDivisions++;
		}
		if(numberDivisions-1 == 0)
			return lastDigit;
		long toMultiply = 1;
		for(long i = 0; i < numberDivisions -1;i++) {
			toMultiply *= 10;
		}
		return  toMultiply * lastDigit;
	}

	/**
	 * M�todo que genera un matriz que representa las X que se van a poner en la grafica de deuda acumulada
	 * pre: los datos de las infracciones de los diferentes meses se han cargado  
	 * @return matriz que representa las X en la grafica de deuda acumulada
	 */
	public long[] datosDeudaAcumulada() {
		LocalDate particion[][] = particiones();
		long ans[] = new long[5];
		long total = totalDebt(particion[0][0], particion[0][1]);
		long first = numberGenerator(total);
		ans[0] = first;
		ans[1] = 1;
		for(int i = 1; i < particion.length; i++) {
			total += totalDebt(particion[i][0], particion[i][1]);
			ans[i+1] = (long)(total/first);
		}
		return ans;
	}


	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora) {
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
}
